import { createClient } from '@supabase/supabase-js'

const supabaseUrl = import.meta.env.VITE_SUPABASE_URL as string
const supabaseAnonKey = import.meta.env.VITE_SUPABASE_ANON_KEY as string

export const supabase = createClient(supabaseUrl, supabaseAnonKey)

export async function session() {    
    // const { data, error } = await supabase.auth.getSession()
    const { data, error } = await supabase.auth.getUser()
    if(error) {
        console.log("get session error", error)
    }
    return data
}