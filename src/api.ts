import { supabase } from './supabase';
import { store } from './store';

export async function loadNewsSourceOptions() {
    try {
        if (!store.user) {
            return;
        }

        let { data, error, status } = await supabase
            .from("news_source")
            .select(`id, news_source_name, news_source_link, sort, link_as_description`)
            .eq("user_id", store.user.id)
            .order("sort")
            .order("news_source_name")

        if (error && status !== 406) throw error

        if (data) {
            store.newsSourceOptions = data || [];
        }
    } catch (error: any) {
        alert(error.message)
    } finally {
    }
}

export async function loadNewsSourceByIndex(newsSourceIndex: number) {
    if (store.newsSourceOptions.length < 1) {
        await loadNewsSourceOptions();
    }
    if (store.newsSourceOptions[newsSourceIndex]) {
        store.newsSourceActive = newsSourceIndex;
        store.newsSourceResults = [];
        const response = await fetch(import.meta.env.VITE_SUPABASE_FUNCTIONS_URL + '/rss',
            {
                method: 'POST',
                headers: {
                    'Authorization': 'Bearer ' + import.meta.env.VITE_SUPABASE_ANON_KEY
                },
                body: JSON.stringify({ "source": store.newsSourceOptions[newsSourceIndex].news_source_link })
            });
        const data = await response.json();
        store.newsSourceResults = data;
    }
}

export async function loadArticle(link: string) {
    store.articleContent = null;
    const response = await fetch(import.meta.env.VITE_SUPABASE_FUNCTIONS_URL + '/reader',
        {
            method: 'POST',
            headers: {
                'Authorization': 'Bearer ' + import.meta.env.VITE_SUPABASE_ANON_KEY
            },
            body: JSON.stringify({ "source": link })
        });
    const data = await response.json();
    store.articleContent = data;
}