import * as Router from 'vue-router'
import Home from './page/Home.vue';
import Article from './page/Article.vue';
import Account from './page/Account.vue';

const routes: any = [
    { path: '/', component: Home },
    { path: '/news/:source', component: Home },
    { path: '/news/:source/article', component: Article },
    { path: '/account', component: Account },
];

const history = Router.createWebHistory(import.meta.env.VITE_BASE_PATH?.toString());

export const router = Router.createRouter({
    history,
    routes,
    scrollBehavior(to, from, savedPosition) {
        if (to.hash) {
            return {
                el: to.hash
                // , offset: { x: 0, y: 10 }
            }
        }
        if (savedPosition) {
            return savedPosition;
        } else {
            return { left: 0, top: 0 };
        }
    }
});
