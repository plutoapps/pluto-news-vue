import { User } from "@supabase/gotrue-js"
import { reactive } from "vue"

type StoreShape = {
    user: User | null | undefined,
    newsSourceOptions: NewsSourceOption[],
    newsSourceActive: number | null,
    newsSourceResults: NewsSourceResult[],
    articleContent: NewsSourceContent | null
}

export const store = reactive<StoreShape>({
    user: null,
    newsSourceOptions: [],
    newsSourceActive: null,
    newsSourceResults: [],
    articleContent: null
})
