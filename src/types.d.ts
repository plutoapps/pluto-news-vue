type NewsSourceOption = {
    id: number
    news_source_name: string
    news_source_link: string
    sort: number,
    link_as_description: boolean
}

type NewsSourceResult = {
    title: string
    link: string
    pubDate: string
    content: string
    contentSnippet: string
    isoDate: string
}

type NewsSourceContent = {
    title: string
    byline: string
    content: string
    textContent: string
    length: number
    excerpt: string
    siteName: string
}