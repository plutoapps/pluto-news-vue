import { serve } from "https://deno.land/std@0.131.0/http/server.ts";
import { corsHeaders } from '../_shared/cors.ts';

import Readability from "./Readability.js";
import { JSDOM } from "https://jspm.dev/npm:jsdom-deno@19.0.1";

serve(async (req) => {

  if (req.method === 'OPTIONS') {
    return new Response('ok', { headers: corsHeaders })
  }

  const { source } = await req.json()
  const response = await fetch(source);
  const body = await response.text();
  const doc = new JSDOM(body, { url: source });

  const reader = new Readability(doc.window.document, {debug:true});
  const article = reader.parse();

  return new Response(
    JSON.stringify(article),
    {
      headers: {
        ...corsHeaders,
        "Content-Type": "application/json"
      }
    },
  )
})
