import { serve } from "https://deno.land/std@0.131.0/http/server.ts"
import { corsHeaders } from '../_shared/cors.ts'

import Parser from "https://esm.sh/rss-parser?no-check";
const parser = new Parser();

serve(async (req) => {

  if (req.method === 'OPTIONS') {
    return new Response('ok', { headers: corsHeaders })
  }

  const { source } = await req.json()
  let feed = await parser.parseURL(source);
  const data = feed.items.slice(0, 20);

  return new Response(
    JSON.stringify(data),
    {
      headers: {
        ...corsHeaders,
        "Content-Type": "application/json"
      }
    },
  )
})
