
Dev vue

```
yarn
yarn dev
```

Build vue

```
yarn
yarn build
```

Run server

```
yarn
yarn build
node server.js
```

`.env.local`

```
VITE_BASE_PATH=/
VITE_SUPABASE_URL=https://example.supabase.co
VITE_SUPABASE_ANON_KEY=123...abc
VITE_SUPABASE_FUNCTIONS_URL=https://example.functions.supabase.co
VITE_AUTH_REDIRECT_URL=http://localhost:3000/
```

```
./node_modules/supabase/bin/supabase functions deploy rss
./node_modules/supabase/bin/supabase functions deploy reader
```