import { defineConfig } from 'vite'
import vue from '@vitejs/plugin-vue'

// https://vitejs.dev/config/
export default defineConfig({
  plugins: [vue()],
  base: process.env.VITE_BASE_PATH,
  server: {
    host: '0.0.0.0',
    proxy: {
      '/rss': 'http://localhost:3000',
      '/reader': 'http://localhost:3000',
    }
  }
})
