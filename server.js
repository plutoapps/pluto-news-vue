const express = require('express');
const app = express();
const port = process.env.PORT || 3000;
const Parser = require('rss-parser');
const parser = new Parser();
const cors = require('cors');
const { Readability } = require('@mozilla/readability');
const { JSDOM } = require('jsdom');
const fetch = require('node-fetch');

app.use(cors());

app.use('/', express.static('dist'));

app.get('/rss', async (req, res) => {
    const source = req.query.source;
    let feed = await parser.parseURL(source);
    res.send(feed.items.slice(0, 20));
});

app.get('/reader', async (req, res) => {
    const source = req.query.source;
    const response = await fetch(source);
    const body = await response.text();
    const doc = new JSDOM(body, { url: source });
    const reader = new Readability(doc.window.document);
    const article = reader.parse();
    res.send(article);
});

app.listen(port, () => {
    console.log(`Example app listening at http://localhost:${port}`);
});
